<?php

//tipos básicos

//string
$nome = "Gustavo";
$site = 'www.google.com';

//inteiro, númerico
$ano = 1993;
//float
$salario = 5500.50;
//booleano
$bloqueado = false;

////////////////////////////////////////////////////////

//tipos compostos

//array
$frutas = array("abacaxi", "laranja", "manga");
echo $frutas[2];
echo "<br/>";

//orientação objetos

$nascimento = new DateTime();

var_dump($nascimento);

/////////////////////////////////

//tipos especiais

//resource

$arquivo = fopen("exemplo-03.php", "r");

var_dump($arquivo);

//null

$nulo = NULL;

var_dump($nulo);

?>