<?php

require_once("vendor/autoload.php");

//$app = AppFactory::create();

$app = new \Slim\Slim();

$app->get('/', function(){

    echo json_encode(array(
        'date' => date("Y-m-d H:i:s")
    ));
    
});

/*
$app->get('/hello/{name}', function (Request $request, Response $response, array $args) {
    $name = $args['name'];
    $response->getBody()->write("Hello, $name");
    return $response;
});
*/

$app->run();

?>