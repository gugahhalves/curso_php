<?php

// autoload_static.php @generated by Composer

namespace Composer\Autoload;

class ComposerStaticInit130b55b3f07299a8fafd3b4e224eabb7
{
    public static $prefixesPsr0 = array (
        'S' => 
        array (
            'Slim' => 
            array (
                0 => __DIR__ . '/..' . '/slim/slim',
            ),
        ),
    );

    public static function getInitializer(ClassLoader $loader)
    {
        return \Closure::bind(function () use ($loader) {
            $loader->prefixesPsr0 = ComposerStaticInit130b55b3f07299a8fafd3b4e224eabb7::$prefixesPsr0;

        }, null, ClassLoader::class);
    }
}
