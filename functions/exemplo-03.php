<?php

function ola($texto, $periodo = "Bom dia"){
    return "Olá $texto, <br> $periodo! <br><br>";
}

echo ola("mundo", "bom dia, boa tarde e boa noite");
echo ola("Gustavo", "Boa tarde");
echo ola("Álef", "Boa noite");
echo ola("Victor", "Bom dia");

?>