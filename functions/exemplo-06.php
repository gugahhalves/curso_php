<?php

$pessoa = array(
    'nome' => 'Gustavo',
    'idade' => 26
);
//& passagem de paramentro por referência
foreach ($pessoa as &$value) {
 
    if (gettype($value) === 'integer') $value += 10;

    echo $value . "<br>";

}

print_r($pessoa);

?>