<?php

interface Veiculo {
    
    public function acelerar($velocidade);
    public function frenar($velocidade);
    public function trocarMarcha($marcha);
    
}

abstract class Automovel implements Veiculo //classe abstrata - não deixar instanciar diretamente essa classe, teria que criar uma outra e extender essa.
{
    
    public function acelerar($velocidade){
        
        echo "Veículo acelerou até " . $velocidade . " Km/h";
        
    }
    
    
    public function frenar($velocidade){
        
        echo "Veículo frenou até " . $velocidade . " Km/h";
        
    }
    
    public function trocarMarcha($marcha){
        
        echo "Veículo engatou a " . $marcha . " marcha";
        
    } 
    
    
}


class DelRey extends Automovel
{
    
    public function empurrar(){
        
        
    }
    
}

$carro = new DelRey();
$carro->acelerar(200);



?>