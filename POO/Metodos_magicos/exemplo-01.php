<?php

class Endereco
{
    private $logradouro;
    private $numero;
    private $bairro;
    private $cidade;
    private $estado;
    
    public function __construct($l, $n, $b, $c, $e){
        
        $this->logradouro = $l;
        $this->numero = $n;
        $this->bairro = $b;
        $this->cidade = $c;
        $this->estado = $e;
        
    }

    public function __destruct(){

        var_dump("DESTRUIR");
    }

    public function __toString(){

        return $this->logradouro. ", ". $this->cidade;

    }

}

$meuEndereco = new Endereco("Rua Durval Coelho Normande", "57", "Farol", "Maceió", "Alagoas");

echo $meuEndereco;

var_dump($meuEndereco);

unset($meuEndereco);

?>