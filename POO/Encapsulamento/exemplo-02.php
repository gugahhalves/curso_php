<?php

class Pessoa {
    
    public $nome = "Rasmus Lerdorf"; // todo mundo ver
    protected $idade = 48; // mesma classe e classes extendidas
    private $senha = "123456"; // só a mesma classe
    
    public function verDados(){
        
        echo "Nome: ";
        echo $this->nome . "<br/>";
        echo "Idade: ";
        echo $this->idade . "<br/>";
        echo "Senha: ";
        echo $this->senha . "<br/>";
        
    }
}


class Programador extends Pessoa
{
    
    public function verDados(){
        
        echo get_class($this) . "<br/>"; // mostra de onde está vindo as informações
        
        echo "Nome: ";
        echo $this->nome . "<br/>";
        echo "Idade: ";
        echo $this->idade . "<br/>";
        echo "Senha: ";
        echo $this->senha . "<br/>";
        
    }
    
}


$objeto = new Programador();
$objeto->verDados();

/*
echo $objeto->nome . "<br>";
echo $objeto->idade . "<br>";
echo $objeto->senha . "<br>";
*/

?>