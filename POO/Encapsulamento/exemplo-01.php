<?php

class Pessoa {
    
    public $nome = "Rasmus Lerdorf";
    protected $idade = 48;
    private $senha = "123456";

    public function verDados(){

        echo "Nome: ";
        echo $this->nome . "<br/>";
        echo "Idade: ";
        echo $this->idade . "<br/>";
        echo "Senha: ";
        echo $this->senha . "<br/>";

    }
}

$objeto = new Pessoa();
$objeto->verDados();

/*
echo $objeto->nome . "<br>";
echo $objeto->idade . "<br>";
echo $objeto->senha . "<br>";
*/

?>