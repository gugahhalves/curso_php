<?php

class Documento {


    private $numero;

    /**
     * Get the value of numero
     */ 
    public function getNumero()
    {
        return $this->numero;
    }

    /**
     * Set the value of numero
     *
     * @return  self
     */ 
    public function setNumero($numero)
    {
        $this->numero = $numero;

        return $this;
    }
}

$cpf = new Documento();
$cpf->setNumero("095.399.614-08");

var_dump($cpf->getNumero());

?>