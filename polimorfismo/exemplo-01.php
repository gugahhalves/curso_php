<?php

abstract class Animal {
    
    public function falar(){
        
        return "Som";
        
    }
    
    public function mover(){
        
        return "Anda";
        
    }
}

class Cachorro extends Animal {
    
    public function falar(){
        
        return "Late";
        
    }
    
}

class Gato extends Animal
{
    public function falar(){
        
        return "Miaa";
        
    }

    public function mover(){
        
        return "Escala";
        
    }
}

class Passaro extends Animal
{
    public function falar(){
        
        return "Canta";
        
    }

    public function mover(){
        
        return "Voa e " . parent::mover();
        
    }
}


echo "Comportamentos do Cachorro <br>";

echo "============================= <br>";

$pluto = new Cachorro();
echo $pluto->falar() . "<br>";
echo $pluto->mover() . "<br>";
echo "============================= <br>";
echo "|||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||| <br>";
echo "============================= <br>";

echo "Comportamentos do Gato <br>";
echo "============================= <br>";
$garfield = new Gato();
echo $garfield->falar() . "<br>";
echo $garfield->mover() . "<br>";
echo "============================= <br>";

echo "============================= <br>";
echo "|||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||| <br>";
echo "============================= <br>";

echo "Comportamentos do Passaro <br>";
echo "============================= <br>";
$piupiu = new Passaro();
echo $piupiu->falar() . "<br>";
echo $piupiu->mover() . "<br>";
echo "============================= <br>";





?>