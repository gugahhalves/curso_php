<?php

interface Veiculo {
    
    public function acelerar($velocidade);
    public function frenar($velocidade);
    public function trocarMarcha($marcha);
    
}


class Civic implements Veiculo
{
    
    public function acelerar($velocidade){
        
        echo "Veículo acelerou até " . $velocidade . "Km/h";
        
    }
    
    
    public function frenar($velocidade){
        
        echo "Veículo frenou até " . $velocidade . "Km/h";
        
    }
    
    public function trocarMarcha($marcha){
        
        echo "Veículo engatou a " . $marcha . " marcha";
        
    } 
    
    
}

$carro = new Civic();
$carro->trocarMarcha("segunda");

?>