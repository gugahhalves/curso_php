<?php

class Usuario 
{
    
    private $id_usuario;
    private $deslogin;
    private $dessenha;
    private $dtcadastro;
    
    public function getId_usuario()
    {
        return $this->id_usuario;
    }
    
    public function setId_usuario($id_usuario)
    {
        $this->id_usuario = $id_usuario;
        
        return $this;
    }
    
    public function getDeslogin()
    {
        return $this->deslogin;
    }
    
    public function setDeslogin($deslogin)
    {
        $this->deslogin = $deslogin;
        
        return $this;
    }
    
    public function getDessenha()
    {
        return $this->dessenha;
    }
    
    public function setDessenha($dessenha)
    {
        $this->dessenha = $dessenha;
        
        return $this;
    }
    
    public function getDtcadastro()
    {
        return $this->dtcadastro;
    }
    
    public function setDtcadastro($dtcadastro)
    {
        $this->dtcadastro = $dtcadastro;
        
        return $this;
    }
    
    public function loadById($id){
        
        $sql = new Sql();
        
        $results = $sql->select("SELECT * FROM tb_usuarios WHERE id_usuario = :ID", array(
            ":ID"=>$id
        ));
        
        if (count($results) > 0) {
            
            $this->setData($results[0]);
            
        }
        
    }
    
    public static function getList(){
        
        $sql = new Sql();
        
        return $sql->select("SELECT * FROM tb_usuarios ORDER BY deslogin");
        
    }
    
    public static function search($login){
        
        $sql = new Sql();
        
        return $sql->select("SELECT * FROM tb_usuarios WHERE deslogin LIKE :SEARCH ORDER BY deslogin", array(
            ':SEARCH'=>"%".$login."%"
        ));
        
    }
    
    public function login($login, $password){
        
        $sql = new Sql();
        
        $results = $sql->select("SELECT * FROM tb_usuarios WHERE deslogin = :LOGIN AND dessenha = :PASSWORD", array(
            ":LOGIN"=>$login,
            ":PASSWORD"=>$password
        ));
        
        if (count($results) > 0) {
            
            $this->setData($results[0]);
            
        } else {
            throw new Exception("Login e ou Password inválidos");
        }
    }
    
    public function insert(){
        
        $sql = new Sql();
        $results = $sql->select("CALL sp_usuarios_insert(:LOGIN, : PASSWORD)", array(
            ':LOGIN'=>$this->getDeslogin(),
            ':PASSWORD'=>$this->getDessenha()
        ));
        
        
    }
    
    public function update($login, $password){

        $this->setDeslogin($login);
        $this->setDessenha($password);        

        $sql = new Sql();

        $sql->query("UPDATE tb_usuarios SET deslogin = :LOGIN, dessenha = :PASSWORD WHERE id_usuario = :ID", array(
            ':LOGIN'=>$this->getDeslogin(),
            ':PASSWORD'=>$this->getDessenha(),
            ':ID'=>$this->getId_usuario()
        ));

    }

    public function delete(){

        $sql = new Sql();

        $sql->query("DELETE FROM tb_usuarios WHERE id_usuario = :ID", array(
            ':ID'=>$this->getId_usuario()

        ));

        $this->setId_usuario(0);
        $this->setDeslogin("");
        $this->setDessenha("");
        $this->setDtcadastro(new DateTime());
    }

    public function setData($data){
        
        $this->setId_usuario($data['id_usuario']);
        $this->setDeslogin($data['deslogin']);
        $this->setDessenha($data['dessenha']);
        $this->setDtcadastro(new DateTime($data['dtcadastro']));
        
    }

        
    public function __construct($login = "", $password = ""){

        $this->setDeslogin($login);
        $this->setDessenha($password);

    }
    
    public function __toString(){
        
        return json_encode(array(
            "id_usuario"=>$this->getId_usuario(),
            "deslogin"=>$this->setDeslogin(),
            "dessenha"=>$this->setDessenha(),
            "dtcadastro"=>$this->setDtcadastro()->format("d/m/Y H:i:s")
            
        ));

        if(count($results) > 0){
            $this->setData($results[0]);
        }
        
    }
    
}


?>