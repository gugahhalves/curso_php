<?php

$image = imagecreatefromjpeg("certificado.jpg");

$tileColor = imagecolorallocate($image, 0, 0, 0);
$gray = imagecolorallocate($image, 100, 100, 100);

imagestring($image, 5, 450, 150, "CERTIFICADO", $tileColor);

/*
imagettftext(
    $image, 32, 0, 320, 250, $tileColor,
    "fonts" . DIRECTORY_SEPARATOR . "Bevan" . DIRECTORY_SEPARATOR . "Bevan-Regular.ttf",
    "CERTIFICADO"
);
*/

/*
imagettftext(
    $image, 32, 0, 375, 350, $tileColor,
    "fonts" . DIRECTORY_SEPARATOR . "Playball" . DIRECTORY_SEPARATOR . "Playball-Regular.ttf",
    utf8_decode("Gustavo Henrique Alves Pinto de Góes")
);
*/

imagestring($image, 5, 440, 350, utf8_decode("Gustavo Henrique Alves Pinto de Góes"), $tileColor);

imagestring($image, 3, 450, 370, utf8_decode("Concluído em: ") . date("d/m/Y"), $tileColor);

header("Content-type: image/jpeg");

imagejpeg($image); //, "certificado-".date("Y-m-d") . ".jpg");

imagedestroy($image);

?>