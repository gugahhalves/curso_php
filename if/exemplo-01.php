<?php

$qualASuaIdade = 25;

$idadeCrianca = 12;
$idadeMaior = 18;
$idadeMelhor = 65;

//Primeiro exemplo de If simples onde trás vários resultados
if ($qualASuaIdade <= $idadeCrianca){
    echo "Você é uma criança";
} elseif ($qualASuaIdade >= $idadeCrianca) {
    echo "Você já não é mais uma criança!";
} 
echo "<br/>";
if ($qualASuaIdade >= $idadeMaior) {
    echo "Você é de maior!";
} elseif ($qualASuaIdade <= $idadeMaior) {
    echo "Você é de menor!";
} 
echo "<br/>";   
if ($qualASuaIdade >= $idadeMelhor) {
    echo "Você conseguiu chegar na melhor idade, PARABÉNS!";
} elseif ($qualASuaIdade <= $idadeMelhor) {
    echo "Você está prestes a chegar na melhor idade, aguente firme!";
}
echo "<br/>"; 

//Segundo exemplo de If encadeado onde trás um resultado
if ($qualASuaIdade < $idadeCrianca){
    echo "Você é uma Criança";
} elseif ($qualASuaIdade < $idadeMaior) {
    echo "Você é um Adolecente";
} elseif ($qualASuaIdade < $idadeMelhor) {
    echo "Você é um Adulto";
} else {
    echo "Você é um Idoso";
}

echo "<br/>"; 

echo ($qualASuaIdade < $idadeMaior)?"Menor de idade":"Maior de Idade";

?>